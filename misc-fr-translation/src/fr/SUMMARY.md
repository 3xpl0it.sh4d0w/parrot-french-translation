# Summary

- [Bienvenue dans la documentation ParrotOS](<./bienvenue-dans-la-documentation-parrot.md>)

- [Introduction]()
    - [Qu'est-ce que Parrot ?](<./qu-est-ce-que-parrot.md>)
    - [Qu'est-ce que GNU/Linux ?](<./base-gnu-linux.md>)
    - [Télécharger Parrot](<./telecharger-parrot.md>)
    - [Exigences système](<./exigences-systeme.md>)
    - [Environnement de bureau](<./environnement-de-bureau.md>)
    - [Communauté et réseau sociaux](<./communaute.md>)

- [Installation]()
    - [Installer Parrot](<./installation.md>)
    - [Comment créer une clé d'installation Parrot](<./comment-creer-une-cle-installation.md>)
    - [Parrot en machine virtuel]()
        - [Installer Parrot dans VirtualBox](<./installer-parrot-virtualbox.md>)
        - [VirtualBox Guest additions](<./virtual-box-guest-additions>)
    - [Parrot sur docker](<./parrot-sur-docker.md>)
    - [Parrot dans Qubes OS](<./parrot-qubes-os.md>)

- [Configuration]()
    - [Gestion des programmes sur Parrot](<./gestion-software-parrot.md>)
    - [Driver Nvidia](<./driver-nvidia.md>)
    - [Permissions des dossiers et fichiers](<./permissions-fichier-dossier.md>)
    - [Vérification des clés et hash](<./cle-hash-verification.md>)
    - [Apparmor](<./apparmor.md>)
    - [Usage de docker](<./usage-docker.md>)
    - [Technologies d'assistance](<./technologies-assitance.md>)
    - [Liste des mirroirs](<./liste-mirroirs.md>)    
    
- [Mentions légales]()
    -[Politique de confidentialité](<./politique-confidentialite.md>)
    - [Warrant Canary](<./warrant-canary.md>)