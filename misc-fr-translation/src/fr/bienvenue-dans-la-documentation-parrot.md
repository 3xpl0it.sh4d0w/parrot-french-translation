<h1 align="center">Bienvenue dans la documentation ParrotOS</h1>

Une partie importante de tout système d'exploitation est la documentation,<br> 
les manuels techniques qui décrivent le fonctionnement et l'utilisation des programmes.<br>
Dans le cadre de ses efforts pour créer un système d'exploitation libre de haute qualité,<br>
le projet Parrot s'efforce de fournir à tous ses utilisateurs une documentation appropriée sous une forme facilement accessible.

La documentation est un travail en cours (WIP),<br>
et tous les utilisateurs de Parrot sont invités à contribuer au processus de création et de traduction de ce portail.


Pour l'instant, elle est divisée en trois grands domaines :

- [Présentation](<./what-is-parrot.md>), qui contient toutes les informations de base sur le projet Parrot.

- [Installation](<./installation.md>), où il est expliqué comment installer Parrot sur votre machine physique ou virtuelle, créer un périphérique de démarrage, docker, etc...

- [Configuration](<./parrot-software-management.md>), Vous trouverez ici les aspects plus techniques, des conseils sur la configuration de certains logiciels, la gestion de votre système, etc...



<div style="text-align: center;">
    <a href="https://parrotsec.org/download/"><img src="./images/parrot-4.11.jpg" width="60%"/></a>
</div>


## Cette Documentation ##

Cette documentation a été rendue possible grâce au travail des membres de la communauté Parrot OS.

*English* Doc Team 
- Lorenzo "palinuro" Faletra
- Irene "tissy" Pirrotta
- Dario Camonita
- José Gatica (Responsable de la version espagnole également)
- Patrick Dunn


Anciens contributeurs :
- Eloir Corona
- Adrian "Ghostar" Baldiviezo
- Josu Elgezabal (Responsable de la documentation)
- Romell Marín (Responsable de la documentation)
- Claudio Marcial (Web - SysAdmin)
- Alejandro Pineda (ParrotOS-ES Art)
- Manuel Hernández (ParrotOS-ES Art)
- Raúl Alderete (Matériel audiovisuel)

Si vous souhaitez nous rejoindre et collaborer à ce projet,<br>
nous vous invitons à vous rendre sur notre [Groupe de discussion Telegram](https://t.me/parrotsecgroup), Vous pouvez nous trouver sur notre [Groupe Facebook](https://www.facebook.com/groups/parrotsec) également.

De plus, si vous trouvez une erreur (hé ! nous sommes des humains), vous pouvez écrire un courriel à l'adresse suivante :

`team at parrotsec dot org`

Ou ouvrez une demande de modification dans le [Dépôt de documentation officielle](https://nest.parrotsec.org/org/documentation), il sera examiné et corrigé dès que possible.
