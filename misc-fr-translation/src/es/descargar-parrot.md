# Descargar Parrot OS #

Parrot OS está disponible para descargar [aquí](https://parrotsec.org/download/).

## ¿Qué versión debería elegir? ##

Parrot viene en muchas formas y tamaños para adaptarse a todo el hardware posible y las necesidades de los usuarios.

Según la configuración de hardware y el alcance que tengas, considera estas opciones:

### Parrot 4.11.2 Security Edition ###

Como sugiere el nombre, esta es la edición completa.
Después de la instalación, tendrás una estación de trabajo de pentesting completa lista para usar cargada con una gran variedad de herramientas listas para usar.
Muy recomendado para computadores de escritorio y portátiles con al menos 4 GB de RAM, para una experiencia fluida mientras se realizan múltiples tareas..

[Descargar](https://parrotsec.org/security-edition/)

| Entorno de escritorio |  Tamaño  |
|--------------------|--------|
|<strong>[MATE](<./desktop-enviroments.html#mate-desktop>)</strong>| 4.1 GB |
|<strong>[KDE](<./desktop-enviroments.html#kde-desktop>)</strong>| 4.2 GB |

### Parrot 4.11.2 Home Edition ###
Esta versión de Parrot es una instalación liviana que proporciona las herramientas esenciales necesarias para comenzar a trabajar.
Se basa en los mismos repositorios que la Full Edition, lo que le permite elegir la mayoría de los programas que desea instalar más adelante.
Recomendado para aquellos que están familiarizados con Distros de Pentesting pero que requieren una instalación mínima.

[Descargar](https://parrotsec.org/home-edition/)

| Entorno de escritorio |  Tamaño  |
|--------------------|--------|
|<strong>[MATE](<./desktop-enviroments.html#mate-desktop>)</strong>| 1.9 GB |
|<strong>[KDE](<./desktop-enviroments.html#kde-desktop>)</strong>| 2.0 GB |
|<strong>[XFCE](<./desktop-enviroments.html#xfce-desktop>)</strong>| 1.8 GB |


### Parrot 4.11.2 Security & Home OVA ###
Diseñada para una configuración rápida de VM (máquina virtual), esta versión está diseñada para funcionar en tu software de virtualización favorito.

[Descargar](https://download.parrot.sh/parrot/iso/4.11.1/Parrot-home-4.11.2_virtual.ova.mirrorlist)

| Edición  |  Tamaño  |
|----------|--------|
| Security | 5.5 GB |
| Home     | 2.8 GB |

### Security o Home edition, ¿cuál debería elegir? ###

*Parrot Home Edition y Parrot Security Edition son idénticas, y la única diferencia entre ellas es el conjunto de software que viene preinstalado*.

Parrot OS Home Edition viene sin herramientas de seguridad, mientras que Parrot OS Security Edition viene con todas las herramientas de hacking y pentest preinstaladas.

Puede instalar Parrot Home Edition e instalar solo las herramientas de hacking que realmente necesita, o puede instalarlas todas a la vez con `sudo apt install parrot-tools-full`


### Parrot 4.11.2 en Docker ###
Olvida todo lo que sabes sobre las circunstancias de pentesting. Llevar una computadora portátil a todas partes para realizar tu trabajo ya no es obligatorio.
Ahora puedes tener un VPS remoto cargado con Parrot OS listo para realizar todo tipo de tareas desde un terminal integrado, con discreción.
Esta edición no proporciona una GUI lista para usar, pero está disponible en los repositorios si es necesario.

[Échale un vistazo ahora](<./parrot-en-docker.md>)
