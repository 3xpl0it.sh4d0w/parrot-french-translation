# Software Libre

El "software libre" es un software que respeta la libertad de los usuarios y su comunidad. En términos generales, significa que los usuarios tienen la libertad de ejecutar, copiar, distribuir, estudiar, modificar y mejorar el software. En otras palabras, el "software libre" es una cuestión de libertad, no de precio. Para entender el concepto, piensa en "libre" como "libertad de expresión", no como "cerveza gratis". En inglés, a veces en lugar de "free software" decimos "libre software", usando ese adjetivo en español, derivado de "libertad", para mostrar que no queremos decir que el software sea gratis.

Cuatro son las libertades que definen al "Software Libre":
- **Libertad 0** : La libertad de ejecutar el programa como se desee, para cualquier propósito.

- **Libertad 1**: La libertad de estudiar cómo funciona el programa y cambiarlo para hacer lo que se desee. El acceso al código fuente es una condición necesaria para esto.

- **Libertad 2**: La libertad de redistribuir copias.

- **Libertad 3**: La libertad de distribuir copias de sus versiones modificadas a terceros. Esto le permite ofrecer a toda la comunidad la oportunidad de beneficiarse de las modificaciones. El acceso al código fuente es una condición necesaria para esto.

\
Un programa es "software libre" si otorga apropiadamente a los usuarios todas estas libertades. De lo contrario, no es gratis. Se dice que es "Software propietario" o "Software privativo".

A modo de resumen podríamos decir que:
- "Software Libre" o "Libre Software" no significa necesariamente que sea gratuito, aunque en muchos casos lo es.
- El "software libre" proporciona cuatro libertades básicas: libertad para ejecutar software, libertad para modificar y estudiar su código, libertad para redistribuir copias de dicho software y libertad para distribuir copias de software modificado.


Puedes leer esta información en el siguiente enlace: [https://www.gnu.org/philosophy/free-sw.en.html](https://www.gnu.org/philosophy/free-sw.en.html)


# Proyecto GNU

Empecemos con un poco de historia ... Son los años 70 del siglo XX, cuando un hombre llamado Richard Stallman empezó a trabajar en el MIT (Instituto Tecnológico de Massachusetts). En esta época era muy común trabajar con software libre. Los programadores eran libres de cooperar entre ellos y lo hacían con bastante frecuencia. Es más, incluso las empresas de informática distribuyeron su software libremente. Todo esto cambió en la década de 1980, y prácticamente todo el software comenzó a distribuirse de forma privada, lo que significa que dicho software tenía propietarios que prohibían la cooperación entre usuarios. Por ello, y ante lo que parece una injusticia, Richard Stallman decide crear el proyecto GNU en 1983. Siendo en 1985 cuando se funda la Free Software Foundation con el objetivo de recaudar fondos para ayudar al programa GNU.

El sistema operativo GNU es un completo sistema de software libre compatible con Unix. El término GNU proviene de "GNU no es Unix". Se pronuncia en una sola sílaba: Ñu. Richard Stallman escribió el anuncio inicial del Proyecto GNU en septiembre de 1983. En septiembre de 1985 se publicó una versión ampliada, llamada Manifiesto GNU [1]..

Se eligió el nombre "GNU" porque cumplía con algunos requisitos. En primer lugar, era un acrónimo recursivo de "GNU Is Not Unix". En segundo lugar, era una palabra real. Por último, fue divertido de decir (o cantar) [2].

Decidieron hacer que el sistema operativo fuera compatible con Unix porque el diseño general ya estaba probado y era portátil, y porque la compatibilidad facilitó a los usuarios de Unix cambiar de Unix a GNU.
 
Un sistema operativo similar a Unix incluye un kernel, compiladores, editores, procesadores de texto, software de correo, interfaces gráficas, bibliotecas, juegos y muchas otras cosas. Por todo esto, escribir un sistema operativo completo requiere mucho trabajo..

A principios de 1990 ya se habían encontrado o programado los componentes principales excepto uno, el kernel.

<img src="./images/gnu.png" alt="GNU Project" width="50%"/>

[1]. [https://www.gnu.org/gnu/manifesto.html](https://www.gnu.org/gnu/manifesto.html)

[2]. [http://www.poppyfields.net/poppy/songs/gnu.html](http://www.poppyfields.net/poppy/songs/gnu.html)


# Proyecto Linux

Retrocedamos en la historia, esta vez a 1991.

Por esa época, un estudiante de informática finlandés llamado Linus Torvalds quería crear un sistema operativo similar a minix (que usaba en la universidad), pero eso funcionaría en su nueva computadora con procesador 80386.

Usando el compilador GNU C, Linus Torvalds pronto tuvo una primera versión del Kernel (kernel) capaz de ejecutarse en su computadora. El 25 de agosto de 1991, anunció este sistema en Usenet, en la lista comp.os.minix. Su proyecto rápidamente ganó adeptos y fueron muchos los que se le unieron, y comenzaron a desarrollar para dicho Kernel..

Linus inicialmente lanzó su software bajo su propia licencia, aunque finalmente eligió una licencia GNU GPL en 1992, en parte porque la herramienta C que había usado para compilarlo también era GPL.

El nombre de Linux, para este kernel, se tomó meses después de su publicación, ya que el propio Linus originalmente había querido llamarlo "Freax". De hecho, en la primera versión del kernel, puede ver dentro del archivo MAKE, cómo lo llamó de esta manera. Finalmente Ari Lemmke, quien era uno de los responsables del servidor FTP en la Universidad Tecnológica de Helsinki, colocó los archivos en el servidor bajo el proyecto "Linux" sin consultar a Linus. A Linus no le gustaba este nombre porque lo encontraba demasiado egocéntrico o egoísta.

Finalmente accedió al cambio de nombre y mucho tiempo después en una entrevista, el propio Linus comentó que “era simplemente el mejor nombre que se podía haber elegido."

![tux](./images/tux.png)

# GNU/Linux

La FSF (GNU) estaba desarrollando un núcleo llamado Hurd (todavía en desarrollo). Este núcleo se estaba desarrollando más lentamente de lo que pensaban. Entonces, antes del lanzamiento del kernel de Linux, se adoptó dentro del proyecto.

Entonces, el nombre correcto para el sistema operativo no es Linux, sino GNU/Linux. Hoy en día, cuando la gente habla de Linux, realmente habla de GNU/Linux. [1].

El núcleo en sí es inútil. El kernel es el componente que hace que el software, y por lo tanto el usuario, sea capaz de comunicarse con el hardware. Pero se necesita más que un núcleo para ejecutar una computadora. Es necesario que existan ciertos programas en la parte del usuario. Estos programas pueden o no tener la licencia GPL (GNU).
 

[1]. [https://www.gnu.org/gnu/linux-and-gnu.en.html](https://www.gnu.org/gnu/linux-and-gnu.en.html)