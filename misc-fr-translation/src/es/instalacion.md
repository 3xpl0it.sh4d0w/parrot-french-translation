# Cómo instalar Parrot OS #

Esta es una guía que te ayudará a instalar Parrot (en su más reciente versión) en tu computador paso a paso a través del instalador oficial y que viene por defecto: *Calamares*.

Esta guía aplica para las ediciones [Security](https://parrotsec.org/security-edition) y [Home](https://parrotsec.org/home-edition).

Cualquier problema o algún detalle que se nos haya pasado, por favor repórtalo en nuestro [Foro oficial de Parrot](https://community.parrotsec.org).

Inserta tu medio de instalación en tu computador y a través de la configuración de tu BIOS arranca Parrot. Aparecerá una pantalla con algunas opciones, algunas más avanzadas que otras.

Selecciona **Try/Install** y presiona *Enter*.

<img src="./images/calamares/10.png" width="85%"/>

Espera a que el sistema cargue (sólo unos segundos).

## Bienvenido a Parrot Live ##

Aquí puedes probar Parrot en todo su esplendor, y luego puedes proceder a la instalación.

Haz click en **Install Parrot**:

<img src="./images/calamares/11.png" width="85%"/>

y el instalador, Calamares, se abrirá.


## ¡Comencemos! ##

El siguiente paso es seleccionar el iddioma para el sistema. 
\
Escoge tu idioma. 
\
Haz click en *Next*.

<img src="./images/calamares/12.png" width="85%"/>

Then select your Region and Zone. Click on *Next*.

<img src="./images/calamares/13.png" width="85%"/>

Now, you can select the keyboard layout. There are many variations available, and you can test them where its written *"Type here to test your keyboard"*.

Click on *Next*.

<img src="./images/calamares/14.png" width="85%"/>


### Particionado de disco en Parrot Security ###

Pensamos que para usuarios menos experimentados el particionado guiado en lo recomendado, 40 gb o más es suficiente, a menos que quieras instalar muchos programas o mantener archivos inmensos en tu disco duro.

<img src="./images/calamares/15.png" width="85%"/>

Aquí puedes decidir si disponer de swap o no. Para más información acerca de swap, \
[https://wiki.debian.org/Swap](https://wiki.debian.org/Swap) \
[https://www.kernel.org/doc/html/latest/power/swsusp.html](https://www.kernel.org/doc/html/latest/power/swsusp.html)

<img src="./images/calamares/16.png" width="85%"/>

*Si quieres*, puedes encriptar el sistema añadiendo una contraseña:

<img src="./images/calamares/17.png" width="85%"/>

Selecciona las opciones que creas más útiles para tí y haz click en *Siguiente*.

### Creando una nueva cuenta de usuario ### 

Se te pedirá crear un nuevo usuario, para no complicarnos por ahora hemos elegido **user**.
Puedes agregar cualquier nombre aquí.

Recuerda que esta es la contraseña para acceder a tu cuenta en el SO, recomendamos crear una que sea larga y compleja.

<img src="./images/calamares/18.png" width="85%"/>

Ahora, haz click en *Siguiente*.


### Completando el proceso de instalación ###

Finalmente, un resumen de las opciones elegidas durante el proceso:

<img src="./images/calamares/19.png" width="85%"/>

Puedes decidir entre cambiar las opciones elegidas, y luego continuar, o proceder con la instalación del sistema. Haz click en **Instalar**.

Confirma haciendo click en **Inatalar ahora**

<img src="./images/calamares/20.png" width="85%"/>

¡Y espera a que se complete la instalación!
\
Con un SSD (Sata), debería tomar un par de minutos.

<img src="./images/calamares/21.png" width="85%"/>

**¡Bien hecho! ¡Instalaste con éxito Parrot OS en tu computador!**

<img src="./images/calamares/22.png" width="85%"/>

### Ingresando a Parrot por primera vez ###

Escribe tu contraseña:

<img src="./images/calamares/23.png" width="85%"/>

**¡Bienvenido a Parrot! ¡Felicidades!**

<img src="./images/calamares/24.png" width="85%"/>
