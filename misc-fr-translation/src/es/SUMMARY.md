# Summary

- [Bienvenido(a)a la Documentación de Parrot](<./bienvenido-documentacion-parrot.md>)

- [Introducción]()
    - [Qué es Parrot](<./que-es-parrot.md>)
    - [Qué es GNU/Linux](<./bases-gnu-linux.md>)
    - [Descargar Parrot](<./descargar-parrot.md>)
    - [Entornos de Escritorio](<./entornos-escritorio.md>)
    - [Comunidad y Redes Sociales](<./comunidad.md>)

- [Instalación]()
    - [Instalar Parrot](<./installation.md>)
    - [Cómo crear un dispositivo USB con Parrot](<./how-to-create-a-parrot-usb-drive.md>)
    - [Parrot en Máquina Virtual]()
        - [Instalar Parrot en VirtualBox](<./install-parrot-on-virtualbox.md>)
        - [Virtualbox Guest Additions](<./virtualbox-guest-additions.md>)
    - [Parrot en Docker](<./parrot-on-docker.md>)
    - [Dualboot con Windows](<./dualboot-with-windows.md>)
    - [Parrot en QubesOS](<./parrot-on-qubesos.md>)

- [Configuración]()
    - [Administración de Software en Parrot](<./parrot-software-management.md>) 
    - [Permisos de Archivos y Directorios](<./file-and-directory-permissions.md>)
    - [Verificación de Hash y Clave](<./hash-and-key-verification.md>)
    - [AppArmor](<./apparmor.md>)
    - [Uso general en Docker](<./general-usage-docker.md>)
    - [Tecnologías de Asistencia](<./assistive-technologies.md>)
    - [Lista Mirrors](<./mirrors-list.md>)
<!-- 
- [Troubleshooting]()
- [F.A.Q.]()
-->
- [Legal]()
    - [Política de Privacidad](<./privacy-policy.md>)
    - [Warrant Canary](<./warrant-canary.md>)


<!-- 
- [Compile a custom kernel](<./19.- Compile a custom kernel.md>)
- [What is Live Mode](<./04.- What is Live Mode.md>)
- [How to create a Live boot device](<./05.- How to create a Parrot USB drive.md>)
    - [How to boot](<./06.- How to boot.md>)
    - [Parrot USB Live Persistence](<./07.- Parrot USB Live Persistence.md>)
-->
<!--
- [Change MySQL - PostgreSQL Password](<./12.- Change MySQL - PostgreSQL Password.md>)
- [Supported WiFi devices](<./13.- Supported WiFi devices.md>)
- [Using a Nvidia GPU on Parrot]()
    - [Nvidia drivers](<./14.- Nvidia drivers.md>)
    - [Nvidia driver install](<./16.- Nvidia driver install.md>)
- [Metasploit Framework](<./17.- Metasploit Framework.md>)
- [Anonsurf](<./18.- Anonsurf.md>)
-->
<!--
- [Mirrors List](<./20.- Mirrors List.md>)
- [General information about GNU/Linux]()
    - [Boot of a Linux System](<./Boot of a Linux System.md>)
    - [GNU/Linux Distributions](<./GNU-Linux Distributions.md>)
    - [Hierarchy of Filesystem and Files](<./Hierarchy of Filesystem and Files.md>)
    - [Installation procedure of a Lemp Nginx web server in ParrotSec](<./Installation procedure of a Lemp Nginx web server in ParrotSec.md>)
    - [Intro to GNU/Linux Security](<./Intro to GNU-Linux Security.md>)
    - [Logs Under Control](<./Logs Under Control.md>)
    - [Network Configuration - Parrot](<./Network Configuration - Parrot.md>)
    - [Reverse Engineering](<./Reverse Engineering.md>)
    - [Services](<./Services.md>)
    - [Shell and Basic Commands](<./Shell and Basic Commands.md>)
    - [su sudo](<./su_sudo.md>)
    - [The Hackers](<./The Hackers.md>)
    - [Users and Groups](<./Users and Groups.md>)
    - [What is Pentesting](<./What is Pentesting.md>)
-->