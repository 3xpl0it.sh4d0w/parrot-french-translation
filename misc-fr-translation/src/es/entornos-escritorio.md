# Entornos de Escritorio #

Parrot OS, además de estar disponible en las ediciones Security y Home, también utiliza varios **entornos de escritorio (DE, por su sigla en inglés)**. Cada DE tiene su peculiaridad, pero recomendamos probarlos antes de decidir qué instalar (sin embargo, ten en cuenta que puedes instalar varios DE en un sistema operativo). 

Al tratarse de una interfaz gráfica a través de la cual el usuario puede interactuar con el sistema operativo, las posibilidades de modificar los distintos componentes del DE son múltiples. Cada uno de los siguientes DE da la posibilidad de personalizarse según los gustos de cada uno.

La diferencia entre los tres DE con soporte oficial se refiere principalmente a aspectos gráficos.

¡Siéntete libre de [descargar](https://parrotsec.org/download/) le edición que te sea útil y con el DE que más te guste! 

## Escritorio MATE ##

[Descargar Home Edition](https://download.parrot.sh/parrot/iso/4.11.2/Parrot-home-4.11.2_amd64.iso)
\
[Descargar Security Edition](https://download.parrot.sh/parrot/iso/4.11.2/Parrot-security-4.11.2_amd64.iso)

Si tienes otro DE, puedes instalar MATE con:
\
`sudo apt install parrot-mate`

<img src="./images/DE/mate.png"/>

## Escritorio KDE ##

[Descargar Home Edition](https://download.parrot.sh/parrot/iso/4.11.2/Parrot-kde-home-4.11.2_amd64.iso)
\
[Descargar Security Edition](https://download.parrot.sh/parrot/iso/4.11.2/Parrot-kde-security-4.11.2_amd64.iso)

Si tienes otro DE, puedes instalar KDE con:
\
`sudo apt install parrot-kde`

<img src="./images/DE/kde.png"/>

## Escritorio XFCE ##

[Descargar Home Edition](https://download.parrot.sh/parrot/iso/4.11.2/Parrot-xfce-4.11.2_amd64.iso)

Si tienes otro DE, puedes instalar XFCE con:
\
`sudo apt install parrot-xfce`

<img src="./images/DE/xfce.png"/>

