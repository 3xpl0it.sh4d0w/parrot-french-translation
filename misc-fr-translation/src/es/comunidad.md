# Portal de la comunidad #

El Proyecto Parrot es un proyecto de código abierto impulsado por la comunidad y una puerta de entrada para difundir ideas innovadoras que nacen dentro de su comunidad.

### Foro de la comunidad ###

En nuestro foro puedes hablar y reportar cualquier problema relacionado con Parrot, compartir información con otros usuarios, interactuar directamente con los desarrolladores de Parrot., etc...

<a href="https://community.parrotlinux.org"><strong>Únete al foro de nuestra comunidad</strong></a>

### Chatea con nosotros en Matrix/Riot.im ###

Únete a <a href="https://matrix.to/#/#parrot:matrix.org?via=matrix.org" target="_blank" class="btn btn-primary">#parrot:matrix.org</a>
desde <a target="_blank" href="https://riot.im">Riot.im</a> o cualquier otro <a target="_blank" href="https://matrix.org">software compatible con Matrix</a>

### Redes sociales ###

- #### Español ####

<a href="https://www.facebook.com/parrot.es" target="_blank">Facebook</a> |
<a href="https://twitter.com/ParrotSec_es" target="_blank">Twitter</a> |
<a href="https://www.youtube.com/c/ParrotSecSchool/videos" target="_blank">Youtube</a> |
<a href="https://t.me/ParrotSpanishGroup" target="_blank">Telegram</a>

- #### Global (inglés) ####

<a href="https://facebook.com/parrotsec" target="_blank">Facebook</a> |
<a href="https://twitter.com/parrotsec" target="_blank">Twitter</a> |
<a href="https://www.youtube.com/channel/UCj2dezzTc_Oy9eAEwBBodpw" target="_blank">Youtube</a> |
<a href="https://telegram.me/parrotproject" target="_blank">Telegram</a> |
<a href="https://www.instagram.com/parrotproject/" target="_blank">Instagram</a>


### Canal IRC ###

Únete a `#parrotsec` en `irc.freenode.net`

Además, puedes acceder a nuestro canal de IRC desde nuestra sala de Matrix.

### Otros grupos ###

Nuestra comunidad vive en varios lugares, aquí hay algunos enlaces para mantenerte en contacto con nosotros.

 <a href="https://t.me/parrotsecgroup" target="_blank">Grupo internacional de Telegram (inglés)</a>

\

 <a href="https://t.me/ParrotSecofftopic" target="_blank">Grupo off-topic internacional de Telegram (inglés)</a>

\

<a href="https://www.facebook.com/groups/parrotsec/" target="_blank">Grupo internacional en Facebook</a>

<h3 align="center">Comunidades locales</h3>

| Comunidad | Link |
|------------|--------|
|<strong>Comunidad Árabe</strong>|<a href="https://t.me/ParrotArabic">Telegram</a>|
|<strong>Comunidad China</strong>|<a href="https://t.me/parrotsecCN">Telegram</a>|
|<strong>Comunidad Francesa</strong>|<a href="https://t.me/ParrotSecFrance">Telegram</a>|
|<strong>Comunidad Alemana</strong>|<a href="https://t.me/parrot_os_ger">Telegram</a>|
|<strong>Comunidad Griega</strong>|<a href="https://t.me/parrotosgr">Telegram</a>|
|<strong>Comunidad India</strong>|<a href="https://t.me/parrotsecindia">Telegram</a>|
|<strong>Comunidad Italiana</strong>|<a href="https://t.me/parrotsecita">Telegram</a>|
|<strong>Comunidad Indonesia</strong>|<a href="https://t.me/parrotsecurityindonesia">Telegram</a>|
|<strong>Comunidad Rusa</strong>|<a href="https://t.me/ParrotSecRU">Telegram</a>|
|<strong>Comunidad Turca</strong>|<a href="https://t.me/parrotsecturkey">Telegram</a>|
|<strong>Comunidad Brasileña</strong>|<a href="https://t.me/ParrotOSBR">Telegram</a>|
