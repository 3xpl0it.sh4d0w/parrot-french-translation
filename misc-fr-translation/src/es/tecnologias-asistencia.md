# Tecnologías de Asistencia #

## Teclado en pantalla ##

Para habilitar el teclado en pantalla, sigue estos pasos.

Desde el menú del panel superior: 

<img src="./images/assistive_technologies/1.png"/>

O desde la barra de búsqueda del panel inferior escribe `Tecnologías de asistencia`

Luego `Activar las tecnologías de asistencia -> click en Aplicaciones preferidas -> selecciona Onboard keyboard -> Ejecutar al inicio`. 

<img src="./images/assistive_technologies/2.png" width="90%"/>
