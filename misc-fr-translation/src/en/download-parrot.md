# Download Parrot OS #

Parrot OS is available for download [here](https://parrotsec.org/download/).

The OS also runs on older machines, but it is recommended to consult the system requirements [here](./system-requirements.html).

## Which version should I choose? ##

Parrot comes in a lot of shapes and sizes in order to fit all possible hardware and users' needs.

Depending on what hardware configuration and scope you have, consider these options:

### Parrot 4.11.2 Security Edition ###

As the name suggests, this is the full edition.
After the installation you have a complete out of the box pentesting workstation loaded with a large variety of tools ready to use.
Highly recommended for PC Desktops and Laptops with at least 4GB of RAM, for a smooth experience whilst multitasking.

[Download](https://parrotsec.org/security-edition/)

| Desktop Enviroment |  Size  |
|--------------------|--------|
|<strong>[MATE](<./desktop-enviroments.html#mate-desktop>)</strong>| 4.1 GB |
|<strong>[KDE](<./desktop-enviroments.html#kde-desktop>)</strong>| 4.2 GB |

### Parrot 4.11.2 Home Edition ###
This version of Parrot is a lightweight installation which provides the essential tools needed to start working.
It relies on the same repositories as the Full Edition, letting you choose most of the programs you want to install later on.
Recommended for those who are familiar with Pentesting Distros but require a minimal installation.

[Download](https://parrotsec.org/home-edition/)

| Desktop Enviroment |  Size  |
|--------------------|--------|
|<strong>[MATE](<./desktop-enviroments.html#mate-desktop>)</strong>| 1.9 GB |
|<strong>[KDE](<./desktop-enviroments.html#kde-desktop>)</strong>| 2.0 GB |
|<strong>[XFCE](<./desktop-enviroments.html#xfce-desktop>)</strong>| 1.8 GB |


### Parrot 4.11.2 Security & Home OVA ###
Purposed for a quick VM setup, this version is intended to work on your favorite virtualization software.

[Download](https://download.parrot.sh/parrot/iso/4.11.1/Parrot-home-4.11.2_virtual.ova.mirrorlist)

| Edition  |  Size  |
|----------|--------|
| Security | 5.5 GB |
| Home     | 2.8 GB |

### Security or Home edition, which one should I choose? ###

*Parrot Home Edition and Parrot Security Edition are identical, and the only difference between them is the set of software that comes pre-installed*.

Parrot OS Home Edition comes with no security tools, while Parrot OS Security Edition comes with all the hacking and pentest tools pre-installed.

You can install Parrot Home Edition and install only the hacking tools you actually need, or you can install all of them at once with `sudo apt install parrot-tools-full`


### Parrot 4.11.2 on Docker ###
Forget all you know about pentesting circumstances. Carrying a laptop everywhere you go to accomplish your job is not mandatory anymore.
You can now have a remote VPS loaded with Parrot OS ready to perform all sort of tasks from an embedded terminal, with discretion. 
This edition does not provide a GUI out of the box, but it's available in the repositories if needed.

[Check it out now](<./parrot-on-docker.html>)
