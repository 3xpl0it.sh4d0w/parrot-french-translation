# System requirements #

Although Parrot is a lightweight OS, especially the Home edition, it's worthwhile checking your computer meets the minimum requirements so it works properly and you have a nice smooth experience when using the distro. 

## Minimum requirements ##

### Parrot Home Edition ###

**CPU**: `Dual core x86_64 Processor or better` \
**RAM**: `2 GB DDR2` \
**Storage**: `20 GB available space` \

### Parrot Security Edition ###

**CPU**: `Dual core x86_64 Processor or better` \
**RAM**: `2 GB DDR3` \
**Storage**: `40 GB available space` \

## Recommended hardware for both editions ##

**CPU**: `Quad core x86_64 Processor or better` \
**RAM**: `8 GB DDR3` \
**Storage**: `128 GB available space on SSD`
