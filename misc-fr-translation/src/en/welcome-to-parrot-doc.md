
\

<h1 align="center">Welcome to our Documentation</h1>

An important part of any operating system is documentation, the technical manuals which describe the operation and use of programs. As part of its efforts to create a high-quality free operating system, the Parrot Project is making every effort to provide all of its users with proper documentation in an easily accessible form.

The documentation is a continuous Work In Progress (WIP), and all Parrot users are invited to contribute to the creation and translation process of this portal.

At the moment, it is divided into three main areas:

- [Introduction](<./what-is-parrot.md>), containing all the basic information on the Parrot project.

- [Installation](<./installation.md>), where it is shown how to install Parrot on your physical or virtual machine, create a boot device, docker, etc...

- [Configuration](<./parrot-software-management.md>), here you will find the more technical aspects, some tips on how to configure some software, management of your system, etc...

\
\

<div style="text-align: center;">
    <a href="https://parrotsec.org/download/"><img src="./images/parrot-4.11.jpg" width="60%"/></a>
</div>


## This Documentation ##

This documentation has been possible thanks to Parrot OS community members's work.

*English* Doc Team 
- Lorenzo "palinuro" Faletra
- Irene "tissy" Pirrotta
- Dario Camonita
- José Gatica (Responsible for the Spanish version too)
- Patrick Dunn

\

past contributors:
- Eloir Corona
- Adrian "Ghostar" Baldiviezo
- Josu Elgezabal (Documentation Leader)
- Romell Marín (Documentation Leader)
- Claudio Marcial (Web - SysAdmin)
- Alejandro Pineda (ParrotOS-ES Art)
- Manuel Hernández (ParrotOS-ES Art)
- Raúl Alderete (Audiovisual Material)

If you want to join us and collaborate with this project, we invite you to join in our [Telegram chat group](https://t.me/parrotsecgroup). You can find us on our [Facebook group](https://www.facebook.com/groups/parrotsec) too.

Also, if you find some mistake (hey! we're humans), you can write an email to:
\
`team at parrotsec dot org`

Or open a merge request in the [official documentation repository](https://nest.parrotsec.org/org/documentation). it will be analyzed and reviewed as soon as possible.