# Assistive Technologies #

## Onscreen Keyboard ##

To enable the onscreen keyboard please follow these steps.

From the top panel menu: 

<img src="./images/assistive_technologies/1.png"/>

Or from the bottom panel's searchbar write `Assistive Technologies`

Then `Enable assistive technologies -> click on Preferred Applications -> select Onboard keyboard -> Run at start`. 

<img src="./images/assistive_technologies/2.png" width="90%"/>
