# Desktop Enviroments #

Parrot OS, in addition to being available in the Security and Home editions, also uses various **Desktop Enviroments (DE)**. Each DE has its  peculiarity, but we recommend trying them out before deciding what to install (however, keep in mind that you can install multiple DEs on one OS). 

Being a graphical interface through which the user can interact with the operating system, the possibilities to modify the various components of the DE are many. Each of the following DE gives the possibility to be customized according to one's tastes.

The difference between the three officially supported DE mainly concerns graphic aspects.

Feel free to [download](https://parrotsec.org/download/) the edition that is useful to you and with the DE that you like the most! 

<div class="panel panel-info">
  <div class="panel-heading">
    <i class="fa fa-info-circle badge" aria-hidden="true"></i>

**Note**

  </div>
  <div class="panel-body">
  It may be useful to know that the user can install more DE on their Parrot, just type in a terminal:
        
      sudo apt update && sudo apt install parrot-<desktop environment>

  then restart your computer. 
  In the login session you can change DE by clicking on the white dot ⚪️ (it's the "default session") and change DE. You can now use the newly installed DE with all the tools and configurations already present previously.
  </div>
</div>

## MATE Desktop ##

[Download Home Edition](https://download.parrot.sh/parrot/iso/4.11.2/Parrot-home-4.11.2_amd64.iso)
\
[Download Security Edition](https://download.parrot.sh/parrot/iso/4.11.2/Parrot-security-4.11.2_amd64.iso)

If you have another DE, you can install MATE by:
\
`sudo apt install parrot-mate`

<img src="./images/DE/mate.png"/>

## KDE Desktop ##

[Download Home Edition](https://download.parrot.sh/parrot/iso/4.11.2/Parrot-kde-home-4.11.2_amd64.iso)
\
[Download Security Edition](https://download.parrot.sh/parrot/iso/4.11.2/Parrot-kde-security-4.11.2_amd64.iso)

If you have another DE, you can install KDE by:
\
`sudo apt install parrot-kde`

<img src="./images/DE/kde.png"/>

## XFCE Desktop ##

[Download Home Edition](https://download.parrot.sh/parrot/iso/4.11.2/Parrot-xfce-4.11.2_amd64.iso)

If you have another DE, you can install XFCE by:
\
`sudo apt install parrot-xfce`

<img src="./images/DE/xfce.png"/>

