# How to install Parrot OS #

This is a guide that will help you install Parrot OS (latest version) on your computer step-by-step through the default and official installer: *Calamares*. 

This guide applies to both the [Security](https://parrotsec.org/security-edition) and [Home](https://parrotsec.org/home-edition) Editions.

Any problems or missing details, please report it to the official [Parrot forum](https://community.parrotsec.org).

Insert your installation media into your computer and through your BIOS settings start Parrot. A screen will appear with several options, including some more advanced ones. 

Select **Try/Install** and press *Enter*.

<img src="./images/calamares/10.png" width="85%"/>

Wait for the OS to load (few seconds).

## Welcome in Parrot Live ##

Here you can test the OS in its entirety, then you can proceed with the installation.

Click on **Install Parrot**:

<img src="./images/calamares/11.png" width="85%"/>

and the installer, Calamares, will start.


## Let's start! ##

The next step is selecting the system's language. 
\
Choose your language. 
\
Click on *Next*.

<img src="./images/calamares/12.png" width="85%"/>

Then select your Region and Zone. Click on *Next*.

<img src="./images/calamares/13.png" width="85%"/>

Now, you can select the keyboard layout. There are many variations available, and you can test them where its written *"Type here to test your keyboard"*.

Click on *Next*.

<img src="./images/calamares/14.png" width="85%"/>


### Parrot Security disk partitioning ###

We think guided partitioning for less experienced users is recommended, 40 GB or more is enough, unless your going to want to install a lot of programs or keep larger file on your hard drive.

<img src="./images/calamares/15.png" width="85%"/>

Here you can decide whether to enable swap or not. For more information about swap, \
[https://wiki.debian.org/Swap](https://wiki.debian.org/Swap) \
[https://www.kernel.org/doc/html/latest/power/swsusp.html](https://www.kernel.org/doc/html/latest/power/swsusp.html)

<img src="./images/calamares/16.png" width="85%"/>

*If you want*, you can also encrypt the system by adding a passphrase:

<img src="./images/calamares/17.png" width="85%"/>

Select the options that you think will be most useful to you and click on *Next*.

### Creating a new user account ### 

You will be asked to create a new user, for simplicity we have chosen a **user**.
You can enter any name in here.

Remember that it is the password to access your OS account, we recommend you to create a long and complex one.

<img src="./images/calamares/18.png" width="85%"/>

Then, click on *Next*.


### Completing the installation process ###

Finally, a summary of the choices made during the procedure:

<img src="./images/calamares/19.png" width="85%"/>

You can decide whether to change the chosen settings, and then go back, or proceed with the installation of the system. Click on **Install**.

Confirm by clicking **Install now**

<img src="./images/calamares/20.png" width="85%"/>

And wait for the installation to complete!
\
With an SSD (Sata), it will take a few minutes.

<img src="./images/calamares/21.png" width="85%"/>

**Well done! You have successfully installed Parrot OS on your computer!**

<img src="./images/calamares/22.png" width="85%"/>

### Login to Parrot for the first time ###

Enter your Password:

<img src="./images/calamares/23.png" width="85%"/>

**Welcome to Parrot! Congrats!**

<img src="./images/calamares/24.png" width="85%"/>
