# Community Portal #

The Parrot Project is a community-driven opensource project, and a gateway to spread innovative ideas born inside it's community.

### Community Forum ###

In our forum you can talk and report any problem related to Parrot, share information with other users, interface directly with the Parrot developers, etc...

<a href="https://community.parrotlinux.org"><strong>Join in our Parrot Community Forum</strong></a>

### Chat with us on Matrix/Riot.im ###

Join <a href="https://matrix.to/#/#parrot:matrix.org?via=matrix.org" target="_blank" class="btn btn-primary">#parrot:matrix.org</a>
from <a target="_blank" href="https://riot.im">Riot.im</a> or any other <a target="_blank" href="https://matrix.org">matrix-compatible software</a>

### Social Pages ###

<a href="https://facebook.com/parrotsec" target="_blank">Facebook</a> |
<a href="https://twitter.com/parrotsec" target="_blank">Twitter</a> |
<a href="https://www.youtube.com/channel/UCj2dezzTc_Oy9eAEwBBodpw" target="_blank">Youtube</a> |
<a href="https://telegram.me/parrotproject" target="_blank">Telegram</a> |
<a href="https://www.instagram.com/parrotproject/" target="_blank">Instagram</a>


### IRC Channel ###

Join `#parrotsec` on `irc.freenode.net`

Also, our IRC channel can be accessed from our Matrix room.

### Other Groups ###

Our community lives in several places, here are some links to stay in touch with us

 <a href="https://t.me/parrotsecgroup" target="_blank">Telegram International Group</a>

\

 <a href="https://t.me/ParrotSecofftopic" target="_blank">Telegram International Offtopic group</a>

\

<a href="https://www.facebook.com/groups/parrotsec/" target="_blank">Facebook International group</a>

<h3 align="center">Local Communities</h3>

| Community | Link |
|------------|--------|
|<strong>Arabic Community</strong>|<a href="https://t.me/ParrotArabic">Telegram</a>|
|<strong>Chinese Community</strong>|<a href="https://t.me/parrotsecCN">Telegram</a>|
|<strong>French Community</strong>|<a href="https://t.me/ParrotSecFrance">Telegram</a>|
|<strong>German Community</strong>|<a href="https://t.me/parrot_os_ger">Telegram</a>|
|<strong>Greek Community</strong>|<a href="https://t.me/parrotosgr">Telegram</a>|
|<strong>Indian Community</strong>|<a href="https://t.me/parrotsecindia">Telegram</a>|
|<strong>Italian Community</strong>|<a href="https://t.me/parrotsecita">Telegram</a>|
|<strong>Indonesian Community</strong>|<a href="https://t.me/parrotsecurityindonesia">Telegram</a>|
|<strong>Spanish Community</strong>|<a href="https://t.me/ParrotSpanishGroup">Telegram</a>|
|<strong>Russian Community</strong>|<a href="https://t.me/ParrotSecRU">Telegram</a>|
|<strong>Turkish Community</strong>|<a href="https://t.me/parrotsecturkey">Telegram</a>|
|<strong>Brazilian Community</strong>|<a href="https://t.me/ParrotOSBR">Telegram</a>|
