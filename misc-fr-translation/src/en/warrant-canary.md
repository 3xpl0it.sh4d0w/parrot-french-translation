# Warrant Canary #

signed with:

    Lorenzo "Palinuro" Faletra
    GPG ID: B350 5059 3C2F 7656 40E6  DDDB 97CA A129 F4C6 B9A4
    GPG KEY: https://cdn.palinuro.dev/canary/key-palinuro.gpg
    GPG KEY MIRROR: https://deb.parrot.sh/mirrors/parrot/misc/canary/key-palinuro.gpg
    GPG KEY MIRROR: https://keybase.io/palinuro/pgp_keys.asc
    
    Parrot Archive Keyring
    GPG ID: 813E EFE8 0280 C579 E2A1  F5E6 B56F FA94 6EB1 660A
    GPG KEY: https://cdn.palinuro.dev/canary/key-palinuro.gpg
    GPG KEY MIRROR: https://deb.parrot.sh/mirrors/parrot/misc/canary/key-palinuro.gpg
    
    
![warrant canary](./images/warrant-canary.png)

Warrant Canary, July 1 2021


    -----BEGIN PGP SIGNED MESSAGE-----
    Hash: SHA512
    
    
    
    Signed Warrant Canary n.3: no incidents or warrants as of July 1 2021
    
    
    This page is to inform users that Parrot Security has not been served with a
    secret government subpoena for its servers (Parrot Project, Parrot Security CIC,
    Lorenzo Faletra or other directly involved partners), software
    (Parrot OS and its official derivatives), or services
    (community services, gitlab instance, email service, cryptpad service,
    cloud platform, CDN nodes, hosted portals etc).
    
    If a warrant canary has not been updated in the time period specified by
    Parrot Security, or if this page disappears, users are to assume that Parrot Project
    has indeed been served with a secret subpoena.
    
    The intention is to allow Parrot Security to warn users of the existence of a
    subpoena passively, without disclosing to others that the government has sought
    or obtained access to information or records under a secret subpoena.
    
    Warrant Canaries have been found to be legal by the United States Justice Department,
    so long as they are passive in their notifications.
    
    This message is signed with the GPG keys of the Parrot OS archive keyrings and
    the Team Leader (and actual legal holder) of Parrot Security.
    
    Every new canary update since July 10 2019 will be digitally signed, and older
    versions of the canary will be archived and made available in a public archive.
    
    signed with:
    
    Lorenzo "Palinuro" Faletra
    GPG ID: B350 5059 3C2F 7656 40E6  DDDB 97CA A129 F4C6 B9A4
    GPG KEY: https://keybase.io/palinuro/pgp_keys.asc
    GPG KEY MIRROR: https://deb.parrot.sh/mirrors/parrot/misc/canary/key-palinuro.gpg
    GPG KEY MIRROR: https://cdn.palinuro.dev/canary/key-palinuro.gpg
    
    Parrot Archive Keyring
    GPG ID: 813E EFE8 0280 C579 E2A1  F5E6 B56F FA94 6EB1 660A
    GPG KEY: https://deb.parrot.sh/mirrors/parrot/misc/canary/key-palinuro.gpg
    GPG KEY MIRROR: https://cdn.palinuro.dev/canary/key-palinuro.gpg
    
    
    -----BEGIN PGP SIGNATURE-----
    
    iQIzBAEBCgAdFiEEVk/M+nIERnPO2F+PytEC9nxQpbkFAmDd/NEACgkQytEC9nxQ
    pbnJNA//ccQx9GJtx8XFZMf7c1I4PuW5S1CPCYu2qxjOvjCn6VomuuK6/ceJHgDv
    7dkhgxw7e/O93cRjESTly1nxIuw7WhRvguyDoNNJ0nRRJyD4mfU1Mk7HoQMnk4L9
    5+VoeOlYQhskrZTQG4DD3ObYJNR69o+8A5YiLCQbgH+iREduS5eIaeUtBwlfpM9O
    m1WGbwtcMtiUmNpZil6iCL1GodymuolMGAD+ZTaQu1oXrlM7Sy+OS0TYIXnjJAtt
    wLVVim+yf1lE1zXv2AWQI08h8V5C2Bt6aCOJ6oRGWZ8kYSs4pEx4SSAiSlqx2IB6
    WTFvlZ13P1tYgolUX5vhawP4hGFliTLecTU3EsVrm2hq4dL6rPfQE/Dz1KgkT7lg
    HNSctEQXnAAEdLaw9ARi+A6SXnHOdYYo5aNsnWCgeQVgaFAMAXHFEliyUfIh6Edz
    VqAR6VNhjB8k0W/weAQ+gh1e6psVXY2yGM++t/lFlqb/b+cTT0aXUl86aFt3UT2p
    3NYfyoI1l4A98pzi1ki36O+tKVH8E9xTvYwtxzIctwBnQdkaMHqkHBg4S5tjDtWA
    41vrrE2VikudlWGRevEEwXa9GSuIWnH9cXtPIabdR5HcV6zHEH0DTFCukViDz0Am
    AU7v1/l962wooctTe+uBbcnmdXhrg2lTKIo6norbXjmErEn0N3OJAkcEAQEKADEW
    IQQKkSzeh/mXIjavi1A2OpalzqnqJwUCYN381hMcdGVhbUBwYXJyb3RzZWMub3Jn
    AAoJEDY6lqXOqeona1IQAJG6yiJSVmIgZfmqLmW/xIuvHkgifHcp/K5Z7oqJYjQK
    +HBOoNqgmqOGXv+HSYmHdDZ99FYX+Poe/NkQBpYZf6XNh+bS6irKl1bX6/knmh0l
    Dv2bZcCdVgC6q0Io5u2khiaIUMIn0qLFRjbSi1UVRVVw9ktkJYBCWJXisH7Oohmv
    RK+jFRFLOp91OY7bS/L+SYZ3UpNbEf6guG4gNSG+6Wa95pMDIKErB5CtZzHCWtwi
    E+4352rBKa4tOkXopzkouSkpesSO0rykxiIkqPohXPB2o31LGE6YMUCe4wBC9pqa
    f0qNaqSuRfDlenx1uWuvt88mVT0YpVvy2QjgVU2aXaeU47sJp3ZISnxWkVNCbjMt
    MLwdK7cZ3pbkdkZrsoLFYgi/klwIDrEWkrD54UugJx/n8xuQT8PpC4/pCpKQ4rW1
    sKtb9NlFZFqZcWGDDrCMwC7TWjMFG+S9+rEAjJEqHY1C8XTmEaAZXu+ii5pkfi5U
    qTSqcSI6eWN3RY2vwPnD7+9/uSYmsYF16gtqaDlUdPNN+JApohDTGLfyuP8cOJUH
    4lqEz+Yai/q10Fempxvp/9TtHvRu8r7DidLrKAtOwZ9de5tjzJUm68CEhptiAtXZ
    kQRvm2VwUwRfxRoGlJa3Ci+ejXNwVLpIX6nLC//ujLIoqRQABJcN2ca0fgaC3yTT
    =N+Nf
    -----END PGP SIGNATURE-----
    

[Warrant Canary n.0](https://deb.parrot.sh/mirrors/parrot/misc/canary/warrant-canary-0.txt)

\

[Warrant Canary n.1](https://deb.parrot.sh/mirrors/parrot/misc/canary/warrant-canary-1.txt)

\

[Warrant Canary n.2](https://deb.parrot.sh/mirrors/parrot/misc/canary/warrant-canary-2.txt)

\

[Warrant Canary n.3](https://deb.parrot.sh/mirrors/parrot/misc/canary/warrant-canary-3.txt)

\

FUCK YEAH! WE ARE STILL SAFE :)





